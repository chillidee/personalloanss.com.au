<?php
/**
 * The Header for single posts.
 *
 * @package the7
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?><!DOCTYPE html>
<!--[if lt IE 10 ]>
<html <?php language_attributes(); ?> class="old-ie no-js">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?> class="no-js">
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<?php if ( presscore_responsive() ) : ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php endif; ?>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<!--[if IE]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
		<script>

    $(function() {

    $("#postcode").autocomplete({

    minLength:3, //minimum length of characters for type ahead to begin

    source: function (request, response) {

    $.ajax({

    type: 'POST',

    url: 'https://personalloanss.com.au/auspost/auspost.php', //your server side script

    dataType: 'json',

    data: {

    postcode: request.term

    },

    success: function (data) {

    //if multiple results are returned

    if(data.localities.locality instanceof Array)

    response ($.map(data.localities.locality, function (item) {

    return {

    label: item.postcode + ', ' + item.location,

    value: item.postcode + ', ' + item.location,

    location: item.location, //create custom data attribute for location

    postcode: item.postcode, //create custom data attribute for postcode

    state: item.state //create custom data attribute for state

    }

    }));

    //if a single result is returned

    else

    response ($.map(data.localities, function (item) {

    return {

    label: item.postcode + ', ' + item.location,

    value: item.postcode + ', ' + item.location,

    location: item.location, //create custom data attribute for location

    postcode: item.postcode, //create custom data attribute for postcode

    state: item.state //create custom data attribute for state

    }

    }));

    }

    });

    },

    //callback function when a selection is made

    select: function( event, ui ) {

    $("#field1").val(ui.item.location); //set field1 using custom data attribute location

    $("#field2").val(ui.item.postcode); //set field2 using custom data attribute postcode

    $("#field3").val(ui.item.state); //set field3 using custom data attribute postcode

    }

    });

    });

    </script>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php
do_action( 'presscore_body_top' );

$config = presscore_config();
?>

<div id="page"<?php if ( 'boxed' == $config->get( 'template.layout' ) ) echo ' class="boxed"'; ?>>

<?php
if ( apply_filters( 'presscore_show_header', true ) ) {
	presscore_get_template_part( 'theme', 'header/header', str_replace( '_', '-', $config->get( 'header.layout' ) ) );
	presscore_get_template_part( 'theme', 'header/mobile-header' );
}

if ( presscore_is_content_visible() && $config->get( 'template.footer.background.slideout_mode' ) ) {
	echo '<div class="page-inner">';
}
?>